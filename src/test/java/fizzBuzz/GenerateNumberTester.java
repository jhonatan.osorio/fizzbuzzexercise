package fizzBuzz;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class GenerateNumberTester {

	@Test 
	public void generate1To100() {
		assertEquals(
				NumberGenerator.generateNumbers(1,100)
				.parallelStream()
				.filter(number -> number > 100 || number < 1 )
				.findAny().isPresent(), false
		);
	}
}
